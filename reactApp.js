class Team extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shots: 0,
      score: 0,
    };
  
     this.shotSound = new Audio("shoot.mp3")
     this.scoreSound = new Audio("score.mp3")
  }

  shotHandler = () => {
    let score = this.state.score;
    this.shotSound.play()
    if (Math.random() > 0.5) {
      score += 1;
      setTimeout(() => {
      this.scoreSound.play()
      }, 1250)
    }

    this.setState((state, props) => ({
      shots: state.shots + 1,
      score,
    }));
  };

  render() {
    let percentageDiv
    if (this.state.shots) {
      const percentage = Math.round(
        (this.state.score / this.state.shots) * 100
      )
      percentageDiv = (
        <div>
          <strong>Blast % {percentage}</strong>
        </div>
      )
    }
  
    return (
      <div className="Team">
        <h2>{this.props.name}</h2>

        <div className="identity">
          <img src={this.props.logo} alt={this.props.name} />
        </div>

        <div>
          <strong>Shots:</strong> {this.state.shots}
        </div>

        <div>
          <strong>Score:</strong> {this.state.score}
        </div>
        {percentageDiv}

        <button onClick={this.shotHandler}>Shoot!</button>
      </div>
    );
  }
}

function App(props) {
  return (
    <div className="App">
      <div className="stats">
        <Team
          name="Leo Lions"
          logo= "https://i.ibb.co/qDJTxyD/lion.png" 
        <div className="versus">
          <h1>VS</h1>
        </div>
        <Team
          name="Optimal Octopuses"
          logo="https://i.ibb.co/RNXYwdp/octopus.png" 

        />
      </div>
    </div>
  );
}

//Render the application
ReactDOM.render(<App />, document.getElementById("root"));

    );
  }
}

function App(props) {
  const jaguars = {
    name: "Jungle Jaguars",
    logoSrc: "./assets/images/jag.png.webp",
  };

  const octopi = {
    name: "Optimal Octopi",
    logoSrc: "./assets/images/octopus.png",
  };

  const lions = {
    name: "Leo Lions",
    logoSrc: "./assets/images/lion.png",
  };

  const sharks = {
    name: "Scary Sea Sharks",
    logoSrc: "./assets/images/shark-basketball.jpg",
  };

  return (
    <div className="App">
      <Game
        venue="Oceanic Auditorium"
        homeTeam={octopi}
        visitingTeam={jaguars}
      />
      <Game venue="Serengetti Stadium" homeTeam={lions} visitingTeam={sharks} />
    </div>
  );
}

// eslint-disable-next-line no-undef
ReactDOM.render(<App />, document.getElementById("root"));
